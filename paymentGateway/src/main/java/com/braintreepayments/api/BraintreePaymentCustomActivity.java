package com.braintreepayments.api;

import com.braintreepayments.api.BraintreePaymentActivity;

/**
 * Created by fernandomanuelperezramos on 10/2/16.
 */

public class BraintreePaymentCustomActivity extends BraintreePaymentActivity {
    public static final String EXTRA_CHECKOUT_REQUEST       = BraintreePaymentActivity.EXTRA_CHECKOUT_REQUEST;
    public static final String EXTRA_PAYMENT_METHOD_NONCE   = BraintreePaymentActivity.EXTRA_PAYMENT_METHOD_NONCE;

}
