package com.aip.paymentGateway.android;

import com.braintreepayments.api.BraintreePaymentActivity;
import com.braintreepayments.api.BraintreePaymentCustomActivity;

/**
 * Created by fernandomanuelperezramos on 10/6/16.
 */

public class AIPPay {
    public static final String EXTRA_CHECKOUT_REQUEST = BraintreePaymentCustomActivity.EXTRA_CHECKOUT_REQUEST;
    public static final String EXTRA_PAYMENT_METHOD_NONCE = BraintreePaymentCustomActivity.EXTRA_PAYMENT_METHOD_NONCE;

    public static final String EXTRA_ERROR_MESSAGE = BraintreePaymentActivity.EXTRA_ERROR_MESSAGE;

    public static final int RESULT_DEVELOPER_ERROR = BraintreePaymentActivity.BRAINTREE_RESULT_DEVELOPER_ERROR;

    public static final int RESULT_SERVER_ERROR = BraintreePaymentActivity.BRAINTREE_RESULT_SERVER_ERROR;

    public static final int RESULT_SERVER_UNAVAILABLE = BraintreePaymentActivity.BRAINTREE_RESULT_SERVER_UNAVAILABLE;
}
