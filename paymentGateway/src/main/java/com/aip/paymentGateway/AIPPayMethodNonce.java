package com.aip.paymentGateway.android;

import android.os.Parcelable;

import com.braintreepayments.api.models.PaymentMethodNonce;

/**
 * Created by fernandomanuelperezramos on 10/6/16.
 */

public class AIPPayMethodNonce {

    private PaymentMethodNonce paymentMethodNonce;

    public AIPPayMethodNonce(Parcelable paymentMethodNonce){
        this.paymentMethodNonce = (PaymentMethodNonce) paymentMethodNonce;
    }

    /**
     * Method to obtain the description of the purchase
     * */
    public String getNonce(){
        return this.paymentMethodNonce.getNonce();
    }

    /**
     * Method to obtain the description of the purchase
     */
    public String getDescription(){
        return this.paymentMethodNonce.getDescription();
    }

    /**
     * Method tot obtain the type lalbel of the payment
     */
    public String getTypeLabel(){
        return this.paymentMethodNonce.getTypeLabel();
    }


}
