#AIP Payment Gateway for Android

Library to handle the AIP Payment Gateway for Android Platform. This library contains the necesary class to handle the component, and a backend for testing purpose.


##Requirements

Android 4.2 or higher.

##Version

1.0.0 (Stable Release)


##Getting Started

For use this library, it must add the AIP's maven repository in the Aplication Gradle
```
apply plugin: 'com.android.application'

repositories {
    jcenter()
    maven { url "http://maven.aiplatform.net/" }
}
```


Then, must load the dependency library through the application gradle.
```
dependencies {
    ...
    compile 'com.aip:paymentGateway:1.0.0'
    ...
}
```

Make sure to load and sync the new gradle configuration.

The next step is to load the activity to use using Intent. But you must generate the authentication and the payment nonce before this step.
For generate thoses nonce, you use a backend web server application (like c#, java or php).

<b>In this demo, include a local backend client for testing purpose, please dont use it in this way.</b>

```
AIPPaymentRequest paymentActivity = new AIPPaymentRequest()
        .clientToken(token)
        .setTitle(getString(R.string.cart))
        .setSubtitle(mProduct.description)
        .setAmount(mProduct.getPriceFormatted());

Intent i  = paymentActivity.getIntent(MainActivity.this);
i.putExtra(AIPPay.EXTRA_CHECKOUT_REQUEST, paymentActivity.getExtra());
startActivityForResult(i,DROP_IN_REQUEST);
```

where DROP_IN_REQUEST is 1.


In the activity result, you would receive the data to send to the backend web server

```
...
@Override
protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    if (requestCode == DROP_IN_REQUEST) {
        switch (resultCode) {
            case AIPPaymentRequest.RESULTS.OK:
                AIPPayMethodNonce payMethodNonce = new AIPPayMethodNonce(data.getParcelableExtra(AIPPay.EXTRA_PAYMENT_METHOD_NONCE));
                String nonce = payMethodNonce.getNonce();
                showConfirmView(nonce);
                break;
            case AIPPay.RESULT_DEVELOPER_ERROR:
            case AIPPay.RESULT_SERVER_ERROR:
            case AIPPay.RESULT_SERVER_UNAVAILABLE:
                // handle errors here, a throwable may be available in
                // data.getSerializableExtra(BraintreePaymentActivity.EXTRA_ERROR_MESSAGE)
                break;
            default:
                break;
        }
    }
}
...
```

The `nonce` in `showConfirmView(nonce)` indicates the payment nonce generated to use to make the purchase 